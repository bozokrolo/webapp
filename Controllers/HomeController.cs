﻿
using WebApplication3.ViewModel;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WebApplication3;

namespace MVCContactBook.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<ContactModel> contacts = new List<ContactModel>();
            //here MyContactBookEntities is our datacontext
            using (MyContactBookEntities dc = new MyContactBookEntities())
            {
                var v = (from a in dc.Contacts
                         join b in dc.Countries on a.CountryID equals b.CountryID
                         join c in dc.States on a.StateID equals c.StateID
                         select new ContactModel
                         {
                             ContactID = a.ContactID,
                             FirstName = a.ContactPersonFname,
                             LastName = a.ContactPersonLname,
                             ContactNo1 = a.ContactNo1,
                             ContactNo2 = a.ContactNo2,
                             EmailID = a.EmailID,
                             Country = b.CountryName,
                             State = c.StateName,
                             Address = a.Address,
                             ImagePath = a.ImagePath
                         }).ToList();
                contacts = v;
            }
            return View(contacts);

        }

    }
}
