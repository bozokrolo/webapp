﻿var app = angular.module('myContactBook', ['ngRoute']);
app.config(['$routeProvider', '$locationProvider', '$httpProvider', function ($routeProvider, $locationProvider, $httpProvider) {
    //routing
    $routeProvider
        .when('/home', {
            templateUrl: '/Scripts/ngComponents/ngTemplates/home.html',
            controller:'homeController'
        })
        .when('/savecontact/:id', {
            templateUrl: '/Scripts/ngComponents/ngTemplates/savecontact.html',
            controller: 'saveContactController'
        })
        .when('/', {
            redirectTo: function () {
                return '/home';
            }
        })
        .when('/error', {
            templateUrl: '/Scripts/ngComponents/ngTemplates/error.html',
            controller:'errorController'
        })
    $locationProvider.$html5mode(false);

    
    $httpProvider.defaults.header.post = { 'content-type': 'aplication/json' }

    //error handling
    $httpProvider.interceptors.push(function ($location) {
        return {
            'responseError': function () {
                
                console.log('Response Error');
                $location.path('/error');
            }
        }
    })



}]);