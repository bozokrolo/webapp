using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace WebApplication3
{
    public partial class ContactBookModel : DbContext
    {
        public ContactBookModel()
            : base("name=MYContactBookModel")
        {
        }

        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<State> States { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Contact>()
                .Property(e => e.ContactPersonFname)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.ContactPersonLname)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.ContactNo1)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.ContactNo2)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.EmailID)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.ImagePath)
                .IsUnicode(false);

            modelBuilder.Entity<Country>()
                .Property(e => e.CountryName)
                .IsUnicode(false);

            modelBuilder.Entity<State>()
                .Property(e => e.StateName)
                .IsUnicode(false);
        }
    }
}
